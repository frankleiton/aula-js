function show_6 (){

    // let divItens = document.getElementById('itens');
    // let html = '<ul>'
    
    // for(let i = 0; i < 10; i++){
    //     html += '<li>' + i + '</li>'
    // }
    
    // html += '</ul>'
    // divItens.innerHTML = html

    // const carro = { 
    //     marca: 'Fiat',
    //     modelo: 'Toro',
    //     ano: 2020
    // };
    
    // for(let item in carro){
    //     console.log(carro[item]);
    // }

    // let frutas = ['Maçã', 'Kiwi', 'Morango', 'Banana']

    // for(let i = 0; i < frutas.length; i++){
    //     console.log(frutas[i]);
    // }

    // for(let item of frutas){
    //     console.log(item);
    // }

    // frutas.forEach(item => {
    //     console.log(item);
    // });
    
    // let frutas = new Array('', '','')

    const vendedorFruta = {
        nome: 'Joao',
        idade: 19,
        frutas : [
            'maçã',
            'melancia',
            'graviola',
            'ciriguela'
        ],
        mostrarFrutas : function() {
            this.frutas.forEach(itens => {
                console.log(itens);
            })
        }
    }

    // vendedorFruta.mostrarFrutas();

    // vendedorFruta.frutas[0] -- maçã
    // vendedorFruta.frutas[1] -- melancia
    // vendedorFruta.frutas[2] -- graviola
    // vendedorFruta.frutas[3] -- ciriguela

    // console.log(vendedorFruta.frutas[vendedorFruta.frutas.length-1]);

    // vendedorFruta.frutas.forEach(item => {
    //     console.log(item);
    // })

    // console.log(vendedorFruta);

}
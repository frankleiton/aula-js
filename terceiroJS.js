/**
 * Criar Objeto em JS
 */

const carro = { 
    marca: 'Fiat',
    modelo: 'Toro',
    ano: 2020,
    todosDados: function() {
        return 'MARCA ' + this.marca + " | MODELO " + this.modelo + " | ANO " + this.ano;
    }
};

function mostrarObjeto(){
    console.log(carro.ano);
    console.log(carro.todosDados());
}
function show_4(){

    /**
     * Retornar tamanho da String
     */
    // let nome = 'Joao paulo';
    // console.log(nome.length);
    
    // let nomee = new String('Joao paulo');
    // console.log(nome.length);

    // let nome2 = new String('Joao paulo');
    // console.log(nome2.length);

    // let nome3 = new String('Joao paulo');
    // console.log(nome3.length);

    // if (nome === nomee) {
    //     console.log('igual');
    // }


    /**
     * COMPARAÇÃO == com ===
     * == Compara valores
     * == Compara valores, tipos e unicidade
     */
    //////////////////////////////////

    // let numero = 10;
    // let numero2 = '10';
    // let numero3 = new Number(10); 
    // let numero4 = new Number(10); 
    // let num = new Number(numero4);

    // if (numero === numero2) {
    //     console.log('igual');
    // }

    // if (numero == numero3) {
    //     console.log('igual');
    // }

    // if (numero3 == '10') {
    //     console.log('igual');
    // }

    // if (numero3 === '10') {
    //     console.log('igual');
    // }

    // if (num === numero4) {
    //     console.log('igual');
    // }

    /**
     * Retornar uma parte especifica da string
     */
    // let str = "Apple, Banana, Kiwi";
    // console.log(str.slice(7, 13));
    // console.log(str.slice(0, 5));
    // console.log(str.slice(15, str.length));


    /**
     * Substituir uma palavra em uma string
     */
    // let str = "Meus alunos do SENAI são os Melhores alunos";
    // let str = "111.111.111-11";
    // console.log(str.replace("alunos","meninos"))
    // console.log(str.replace(/alunos/i,"meninos"))
    // console.log(str.replace(/alunos/g,"meninos"))
    // console.log(str.replace(/\./g,""))


    /**
     * Concatnar
     */
    // let str = "Hello" 
    // let str2 = "World"
    // console.log(str.concat(" ", str2));
    // console.log(str + " " + str2)

    /**
     * Dividir uma String em partes
     */
    // let str = "59000a333".split('a');
    // let strConcat = ''
    // strConcat = strConcat.concat(str[0], str[1])
    // console.log(strConcat);
    // console.log(strSplit[1]);

    // let valor = valor
    // valor + 10;

    // let str = "Voce consegue iniciar uma variavel"
    // console.log(str.startsWith("consegu"));
    // console.log(str.startsWith("Voce"));
    // console.log(str.startsWith("voce"));
    // console.log(str.startsWith("consegue" , 5));

}
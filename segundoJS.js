const cpf = '111.111.111-11';
var nome = 'Frankleiton';
var sobrenome = 'Levy';
var num1 = 10, num2 = 20;

function TesteVariaveis(){
    
    if(true){
        sobrenome = 'Alves';
        console.log(nome)
        console.log(sobrenome)
    }  

    /**
     * Operadores de valores
     */
    if (true) {
        console.log(5 + 2)
        console.log(10 - '10')
        console.log(10 * 10)
        console.log('10' / '10')
        console.log(10 % 2)
        console.log(num1 + '20')
    }

    mostraNomeCompleto('Erika', 'Thifany');
}

function mostraNomeCompleto(nome, sobrenome)
{
    console.log(nome + ' ' + sobrenome);
}

